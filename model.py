from peewee import *
import os

db = SqliteDatabase(os.getenv('SQL_LITE_DB'))

LANG=['EN', 'ES', 'CA', 'FR']

class Project(Model):
    project_id = PrimaryKeyField()
    name = CharField(null=False)
    summary = CharField(null=False)
    primary_color = CharField(default='#6eac88')
    secondary_color = CharField(default='#beb03b')
    website = CharField()
    logo_url = CharField()
    primary_auth_credential = IntegerField(null=False)
    secondary_auth_credential = IntegerField(null=False)
    allow_user_auth = BooleanField(null=False)
    welcome_screen_content = CharField()
    show_welcome_screen = BooleanField()
    language = IntegerField(default=1)
    tracker_access_token = CharField()
    tracker_active = BooleanField()
    support_source_addres = CharField()
    user_token_expiration_seconds = IntegerField(default=1800)
    copyright_notice = CharField()
    terms_and_conditions = CharField()
    company_address = CharField()
    social_links = CharField()

    class Meta:
        database = db

def create_tables():
    with db:
        db.create_tables([Project])
