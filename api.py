import tornado.web
import json
import os
from playhouse.shortcuts import model_to_dict
from peewee import *

from model import Project, LANG
from apiAuthenticate import authenticate_api_token
from dataValidation import DataValidation


class MainHandler(tornado.web.RequestHandler):

    @authenticate_api_token(os.getenv('API_TOKEN'))
    def get(self, pk):
        json_data = {}
        try:
            projects = Project.select().where(Project.project_id==pk).dicts()
            if len(projects) == 0:
                self.set_status(404)
            else:
                validate = DataValidation()
                json_data = validate.data_clean(self.request, LANG, projects.get())
                # json_data = self.clean_data(projects.get())
                json_data = json.dumps(json_data)
        except:
            self.set_status(409)
        finally:
            self.write(json_data)

    @authenticate_api_token(os.getenv('API_TOKEN'))
    def put(self, pk):
        json_data = {}
        try:
            project = json.loads(self.request.body)
            validate = DataValidation()
            project = validate.data_clean(self.request, LANG, project)
            # project = self.clean_data(project)
            query = Project.update(**project).where(Project.project_id==pk).execute()
            if query == 0:
                self.set_status(404)
            else:
                projects = Project.select().where(Project.project_id==pk).dicts()
                json_data = json.dumps(projects.get())
        except:
            self.set_status(400)
        self.write(json_data)

    @authenticate_api_token(os.getenv('API_TOKEN'))
    def delete(self, pk):
        json_data = {}
        query = Project.delete().where(Project.project_id==pk).execute()
        if query == 0:
            self.set_status(404)
            self.write(json_data)
            return
        self.set_status(204)

class PostHandler(tornado.web.RequestHandler):

    @authenticate_api_token(os.getenv('API_TOKEN'))
    def post(self):
        result = {}
        try:
            data = json.loads(self.request.body)
            validate = DataValidation()
            data = validate.data_clean(self.request, LANG, data)
            created = Project.create(**data)
            jsonToReturn = model_to_dict(created)
            result['data'] = jsonToReturn
        except IntegrityError as e:
            self.set_status(400)
            result['error'] = str(e)
        except Exception as e:
            self.set_status(400)
            result['error'] = str(e)
        finally:
            self.write(result)
            return 
