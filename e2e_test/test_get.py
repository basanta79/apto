from tornado.testing import AsyncTestCase, AsyncHTTPClient
import tornado
import urllib

prueba = """{
	"project_id": 22,
	"name": "project 14",
	"summary": "Summary of the project",
    "website": "hello",
    "logo_url": "world",
    "primary_auth_credential": 121212,
    "secondary_auth_credential": 232323,
    "allow_user_auth": false,
    "welcome_screen_content": "Tachan",
    "show_welcome_screen": "True",
    "tracker_access_token": "tracker",
    "tracker_active": "True",
	"language": "ES",
	"support_source_addres": "werwerwer",
    "copyright_notice": "copyrigth",
    "terms_and_conditions": "terms",
    "company_address": "company",
    "social_links": "social"
}"""

prueba2 = """{
	"project_id": 22,
	"name": "project 14",
	"summary": "Summary of the project"
    "website": "hello",
    "logo_url": "world",
    "primary_auth_credential": 121212,
    "secondary_auth_credential": 232323,
    "allow_user_auth": false,
    "welcome_screen_content": "Tachan",
    "show_welcome_screen": "True",
    "tracker_access_token": "tracker",
    "tracker_active": "True",
	"language": "ES",
	"support_source_addres": "werwerwer",
    "copyright_notice": "copyrigth",
    "terms_and_conditions": "terms",
    "company_address": "company",
    "social_links": "social"
}"""

class testCase(AsyncTestCase):

    @tornado.testing.gen_test
    def test_get_key_ko(self):
        client = AsyncHTTPClient()
        with self.assertRaises(tornado.httpclient.HTTPError) as context:
            yield client.fetch("http://127.0.0.1:8000/api/v1/project/35",
                                method="GET",
                                headers={'Api-key': "j8TB3y9jbU0dagSSVzyTSlh16EmdX5wEVx9vHMek3Vc++3ObNDarY8QesRkDUW2w"}
                                )
        self.assertAlmostEqual(context.exception.code, 401)

    @tornado.testing.gen_test
    def test_get_key_ok(self):
        client = AsyncHTTPClient()
        response = yield client.fetch("http://127.0.0.1:8000/api/v1/project/1",
                                method="GET",
                                headers={'Api-key': "J8TB3y9jbU0dagSSVzyTSlh16EmdX5wEVx9vHMek3Vc++3ObNDarY8QesRkDUW2w"}
                                )
        self.assertAlmostEqual(response.code, 200)

    @tornado.testing.gen_test
    def test_get_unknow(self):
        client = AsyncHTTPClient()
        with self.assertRaises(tornado.httpclient.HTTPError) as context:
            response = yield client.fetch("http://127.0.0.1:8000/api/v1/project/345",
                                method="GET",
                                headers={'Api-key': "J8TB3y9jbU0dagSSVzyTSlh16EmdX5wEVx9vHMek3Vc++3ObNDarY8QesRkDUW2w"}
                                )
        self.assertAlmostEqual(context.exception.code, 404)

    @tornado.testing.gen_test
    def test_post_ok(self):
        client = AsyncHTTPClient()
        body = prueba
        response = yield client.fetch("http://127.0.0.1:8000/api/v1/project",
                                method="POST",
                                headers={
                                    'Api-key': "J8TB3y9jbU0dagSSVzyTSlh16EmdX5wEVx9vHMek3Vc++3ObNDarY8QesRkDUW2w",
                                    'Content-Type': 'application/json'
                                },                                
                                body=body
        )
        self.assertAlmostEqual(response.code, 200)
    
    @tornado.testing.gen_test
    def test_post_ko(self):
        # body = urllib.parse.urlencode(prueba)
        body = prueba2
        client = AsyncHTTPClient()
        with self.assertRaises(tornado.httpclient.HTTPError) as context:
            yield client.fetch("http://127.0.0.1:8000/api/v1/project",
                                method="POST",
                                headers={
                                    'Api-key': 'J8TB3y9jbU0dagSSVzyTSlh16EmdX5wEVx9vHMek3Vc++3ObNDarY8QesRkDUW2w',
                                    'Content-Type': 'application/json'
                                },    
                                body=body      
        )
        self.assertAlmostEqual(context.exception.code, 400)