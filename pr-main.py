import tornado.ioloop
import tornado.web
import settings
from api import MainHandler, PostHandler

def make_app():
    return tornado.web.Application([
        (r"/api/v1/project", PostHandler),
        (r"/api/v1/project/(\d+)", MainHandler),
    ], debug=True)

if __name__ == "__main__":
    app = make_app()
    app.listen(8000)
    tornado.ioloop.IOLoop.current().start()