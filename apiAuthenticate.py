

class authenticate_api_token(object):

    def __init__(self, f):
        if f is None :
            print('no key loaded')
        self.f = f
    
    def __call__(self, func):
        def wrapped_f(*args):
            api_sent = args[0].request.headers.get('Api-key')
            if api_sent != self.f:
                args[0].set_status(401)
                args[0].write({})
                return
            func(*args)
        return wrapped_f