# APTO Backend code exercise
This application is simple backend wich implements CRUD functinality of a Projects.

## Dependencies
Uses tornado as a HTTP server, SQLite as a database, and peewee as a ORM. 
Python 3.7.2

## How to run
Clone repo
pip install -r requirements.txt
create .env file using .env.example as template
Execute ```>python scripts/db_creation_script.py``` to create database
Now you can run server

## Testing

### Unit testing
Under test folder, a suit of unittest
```  python3 -m unittest discover test -v```

### E2E testing
Under e2e folder, a suit of end 2 end test
```  python3 -m tornado.testing e2e_test.test_get -v```

## End point

[Create](/api/v1/project) ```/api/v1/project```
Method: POST
Body Params:
```
{
	"project_id": integer,
    "name": String,
    "primar_color": String,
    "secondary_color": String,
    "website": String,
    "logo_url": String,
    "primary_auth_credential": integer,
    "secondary_auth_credential": integer,
    "allow_user_auth": Boolean,
    "welcome_screen_content": String,
    "show_welcome_screen": Boolean,
    "language": Integer,
    "tracker_access_token": String,
    "tracker_active": Boolean,
    "support_source_addres": String,
    "user_token_expiration_seconds": integer,
    "copyright_notice": String,
    "terms_and_conditions": String,
    "company_address": String,
    "social_links": String
}
```
Return codes:
 - 200 - OK Record was created
 - 400 - Bad Request


[Retrieve](/api/v1/project/<id>) ```/api/v1/project/<id>```
Method: GET
Body Params: none
Return code: 200 - Ok


[Update](/api/v1/project/<id>) ```/api/v1/project/<id>```
Method: PUT
Body Params: 
```
{
	"project_id": integer,
    "name": String,
    "primar_color": String,
    "secondary_color": String,
    "website": String,
    "logo_url": String,
    "primary_auth_credential": integer,
    "secondary_auth_credential": integer,
    "allow_user_auth": Boolean,
    "welcome_screen_content": String,
    "show_welcome_screen": Boolean,
    "language": Integer,
    "tracker_access_token": String,
    "tracker_active": Boolean,
    "support_source_addres": String,
    "user_token_expiration_seconds": integer,
    "copyright_notice": String,
    "terms_and_conditions": String,
    "company_address": String,
    "social_links": String
}
```
Return codes: 
 - 200 - OK. Object was modified
 - 400 - BAD REQUEST. Data could not be processed.
 - 404 - NOT FOUND. Object to modificate was not found

[Delete](/api/v1/project/<id>) ```/api/v1/project/<id>```
Method: DELETE
Body params: none
Return codes: 
 - 204 - No content. Object was deleted
 - 404 - Not Found
