import os
from pathlib import Path
from dotenv import load_dotenv
from peewee import *


env_path = Path('.')/'.env'
load_dotenv(dotenv_path=env_path)

from model import db
from model import Project

db.create_tables([Project])
