

class DataValidation(object):

    def __init__(self):
        pass
    
    def int_to_language(self, options, data):
        try:
            result = options[data]
        except:
            result='EN'
        finally:
            return result
    
    def language_to_int(self, choices, source):
        if not source is None:
            source = source.upper()
        if source in choices:
            index = choices.index(source)
        else:
            index = 0
        return index

    def data_clean(self, request, choices, data):
        if request.method == 'PUT' or request.method == 'POST' :
            if 'project_id' in data:
                del data['project_id'] 
            data['language'] = DataValidation.language_to_int(self, choices, data.get('language'))
        else:
            data['language'] = DataValidation.int_to_language(self, choices, data.get('language'))
        return data