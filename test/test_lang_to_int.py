import unittest
from dataValidation import *

class TestDataValidationToInt(unittest.TestCase):

    def setUp(self):
        self.LANG_TEST = ['EN', 'ES', 'CA', 'FR']

    def test_ES(self):
        validation = DataValidation()
        result = validation.int_to_language(self.LANG_TEST, 1)
        self.assertEqual(result, 'ES')
    
    def test_FR(self):
        validation = DataValidation()
        result = validation.int_to_language(self.LANG_TEST, 3)
        self.assertEqual(result, 'FR')

    def test_CA(self):
        validation = DataValidation()
        result = validation.int_to_language(self.LANG_TEST, 2)
        self.assertEqual(result, 'CA')
    
    def test_EN(self):
        validation = DataValidation()
        result = validation.int_to_language(self.LANG_TEST, 0)
        self.assertEqual(result, 'EN')

    def test_letters(self):
        validation = DataValidation()
        result = validation.int_to_language(self.LANG_TEST, 'as')
        self.assertEqual(result, 'EN')

    def test_long(self):
        validation = DataValidation()
        result = validation.int_to_language(self.LANG_TEST, 3245234623562)
        self.assertEqual(result, 'EN')
    
    def test_negative(self):
        validation = DataValidation()
        result = validation.int_to_language(self.LANG_TEST, -2)
        self.assertEqual(result, 'CA')
    
    def test_float(self):
        validation = DataValidation()
        result = validation.int_to_language(self.LANG_TEST, 1.4)
        self.assertEqual(result, 'EN')

if __name__ == "__main__":
    unittest.main()
        