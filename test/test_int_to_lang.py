import unittest
from dataValidation import *

class TestDataValidationToInt(unittest.TestCase):

    def setUp(self):
        self.LANG_TEST = ['EN', 'ES', 'CA', 'FR']

    def test_ES(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'ES')
        self.assertEqual(result, 1)
    
    def test_FR(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'FR')
        self.assertEqual(result, 3)

    def test_CA(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'CA')
        self.assertEqual(result, 2)
    
    def test_EN(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'EN')
        self.assertEqual(result, 0)

    def test_empty(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, '')
        self.assertEqual(result, 0)

    def test_long(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'ASDGAERTEVADFVAERGQERGADFGEADFGASDGEARHEA')
        self.assertEqual(result, 0)
    
    def test_es(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'es')
        self.assertEqual(result, 1)
    
    def test_en(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'en')
        self.assertEqual(result, 0)
    
    def test_ca(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'ca')
        self.assertEqual(result, 2)
    
    def test_fr(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, 'fr')
        self.assertEqual(result, 3)
    
    def test_numbes(self):
        validation = DataValidation()
        result = validation.language_to_int(self.LANG_TEST, '1231')
        self.assertEqual(result, 0)

if __name__ == "__main__":
    unittest.main()
        